# Rules
## Rule id
40
## Description
All data which is sent to subsystem controlling USB peripherals (system bus controllers, such as OHCI/UHCI/EHCI) should be properly initialized. Uninitialized data or not properly initialized data can cause failures in both the device controlled by driver and the whole system. The communication with devices connected to USB interface is performed by USB Request Blocks (URB). Which has the following life circle: usb_alloc_urb (...); - allocating memory for URB initialization usb_submit_urb (...); - sending data to Core USB calling callback function of a driver or waiting for &quot;usb_complete_t&quot; notification about successful receive or failure. * usb_free_urb (...); - free URB

  This rule requires that each URB request before submitting it using usb_submit_urb should be properly initialized initialized either by one of initialization functions depending on URB type or by manually filling all required fields (see rule 0041).

## Naming Convention
- 40_2a : Original encoding of the property (dummy set i.e. counter )
- 40_2a_nd : in the encoding of non-determinism

# Interesting Drivers
| driver                                      | nd            | set           | comment                                                                   |
|---------------------------------------------|---------------|---------------|---------------------------------------------------------------------------|
| drivers/net/can/usb/kvaser_usb.ko           | 26.9216449261 | TIMEOUT       | 4 pairs: 587,1007,1059,1350. Verification task seems obviously easy. Why? |
| drivers/usb/class/usblp.ko                  | 26.7186188698 | TIMEOUT       | 2 pairs. Loop.                                                            |
| drivers/usb/storage/uas.ko                  | 27.4539449215 | 390           | unknown environment input.(switch)                                        |
| drivers/net/wireless/at76c50x-usb.ko        | 31.0998828411 | TIMEOUT       | evidently easy task. Why?                                                 |
| drivers/net/wireless/rt2x00/rt2500usb.ko    | 34.1506130695 | TIMEOUT       | implicit transition(callback)                                             |
| drivers/net/wireless/orinoco/orinoco_usb.ko | 36.8159499168 | TIMEOUT       | implicit transition                                                       |
| drivers/net/irda/ks959-sir.ko               | 76.964869976  | 52.807901144  | implicit transition                                                       |
| drivers/net/wireless/ath/ar5523/ar5523.ko   | TIMEOUT       | 69.8342769146 | evidently easy task. Why?       

Note: TIMEOUT>=120s

# Notes

- \*.o.i files are instrumented sources given to seahorn (ldv_common_model.o.i included)
- \*.c files are original driver sources

## encoing socket contract,nd: ab(cd)*e
slow
nested loop:
dependent of inner loop

## encoding a malicious behavior: abcde
fast
loop-independent


## nd+set
slow, nested loop
loop-dependent

## nd+counter
fast
loop-independent




**Goal**: Identify the program structure that significantly affects verification.

**Driver**: kvaser_usb.c, 4 occurrences of fill/submit pairs.

| line | in function                   | time    | comment                 |
|------|-------------------------------|---------|-------------------------|
| 594  | kvaser_usb_simple_msg_async   | 4.50s   |                         |
| 1011 | kvaser_usb_read_bulk_callback | 4.16s   | cannot find crafted bug |
| 1069 | kvaser_usb_setup_rx_urbs      | 160.14s | loop? remove loop ->167.26s                    |
| 1364 | kvaser_usb_start_xmit         | 296.71s |                         |

- Seahorn cannot find bugs in callback()?
- The wrapper code from LDV?


**Call Tree**
```
---kvaser_usb_probe
--kvaser_usb_init_one
-kvaser_usb_set_mode
------kvaser_usb_open
-----**kvaser_usb_setup_rx_urbs**
----**kvaser_usb_read_bulk_callback**(loop, self-callback)
---kvaser_usb_handle_message
--kvaser_usb_rx_can_msg
--kvaser_usb_handle_message
-kvaser_usb_rx_error
**kvaser_usb_simple_msg_async**

-.ndo_start_xmit = kvaser_usb_start_xmit (top-level)
kvaser_usb_start_xmit
```
**Loop Invariant for case 3**

```
Function: kvaser_usb_open
kvaser_usb_open@entry: true
kvaser_usb_open@return.split: true
Function: kvaser_usb_close
kvaser_usb_close@entry: true
kvaser_usb_close@if.end13.split: true
Function: kvaser_usb_start_xmit
kvaser_usb_start_xmit@entry: true
kvaser_usb_start_xmit@ldv_43333: true
kvaser_usb_start_xmit@return.split: true
Function: kvaser_usb_get_berr_counter
kvaser_usb_get_berr_counter@entry: true
kvaser_usb_get_berr_counter@entry.split: true
Function: kvaser_usb_set_mode
kvaser_usb_set_mode@entry: true
kvaser_usb_set_mode@UnifiedReturnBlock.split: true
Function: kvaser_usb_set_bittiming
kvaser_usb_set_bittiming@entry: true
kvaser_usb_set_bittiming@return.split: true
Function: main
main@entry: true
main@ldv_43333.i.i: (select(main@%shadow.mem1.3, @LDV_USB_URB_BUFS)>=0)
main@ldv_43397.i.i.i:
		(main@%ldv_s_kvaser_usb_driver_usb_driver.3.i>=0)
	(select(main@%shadow.mem1.3, @LDV_USB_URB_BUFS)>=0)
main@ldv_43407.i.i:
		(main@%ldv_s_kvaser_usb_driver_usb_driver.3.i>=0)
	(select(main@%shadow.mem1.3, @LDV_USB_URB_BUFS)>=0)
main@ldv_43101.i.i.i.i:
		(main@%ldv_s_kvaser_usb_driver_usb_driver.3.i>=0)
	(select(main@%shadow.mem1.3, @LDV_USB_URB_BUFS)>=0)
main@ldv_43094.i.i.i.i:
		(main@%ldv_s_kvaser_usb_driver_usb_driver.3.i>=0)
	(select(main@%shadow.mem1.3, @LDV_USB_URB_BUFS)>=0)
main@ldv_43101.i.i59.i.i:
		(main@%ldv_s_kvaser_usb_driver_usb_driver.3.i>=0)
	(select(main@%shadow.mem1.3, @LDV_USB_URB_BUFS)>=0)
main@ldv_43094.i.i81.i.i:
		(main@%ldv_s_kvaser_usb_driver_usb_driver.3.i>=0)
	(select(main@%shadow.mem1.3, @LDV_USB_URB_BUFS)>=0)
main@ldv_43365.i.i.i: ((!(main@%retval.0.i118.i.i<=0))||(!(main@%retval.0.i118.i.i>=0)))
main@ldv_43280.i.i.i.i: ((main@%retval.0.i118.i.i<=-1)||(main@%retval.0.i118.i.i>=1))
main@ldv_43150.i.i.i.i.i: ((main@%retval.0.i118.i.i<=-1)||(main@%retval.0.i118.i.i>=1))
main@ldv_43284.i.i.i.i: ((main@%retval.0.i118.i.i<=-1)||(main@%retval.0.i118.i.i>=1))
main@ldv_43369.i.i.i:
		(main@%i.1.i.i.i>=0)
	((main@%retval.0.i118.i.i>=1)||(main@%retval.0.i118.i.i<=-1))
main@ldv_43412.i.i:
		(main@%i.1.i.i>=0)
	(select(main@%shadow.mem1.3, @LDV_USB_URB_BUFS)>=0)
main@ldv_43384.i.i.i:
		(main@%i.0.i113.i.i>=0)
	((main@%call1.i.i.i+(-1*main@%add.ptr.i.i110.i.i))<=-3264)
	(main@%i.1.i.i>=0)
	(select(main@%shadow.mem1.3, @LDV_USB_URB_BUFS)>=0)
main@ldv_43365.i.i175.i:
		(main@%i.0.i.i172.i>=0)
	(select(main@%shadow.mem1.3, @LDV_USB_URB_BUFS)>=0)
main@ldv_43280.i.i.i184.i: (select(main@%shadow.mem1.3, @LDV_USB_URB_BUFS)>=0)
main@ldv_43150.i.i.i.i198.i: (select(main@%shadow.mem1.3, @LDV_USB_URB_BUFS)>=0)
main@ldv_43284.i.i.i208.i: (select(main@%shadow.mem1.3, @LDV_USB_URB_BUFS)>=0)
main@ldv_43369.i.i223.i:
		(main@%i.1.i.i220.i>=0)
	(select(main@%shadow.mem1.3, @LDV_USB_URB_BUFS)>=0)
main@ldv_43472.i: (select(main@%shadow.mem1.3, @LDV_USB_URB_BUFS)>=0)
main@verifier.error.split: false

```

```
kvaser_usb_probe( var_group3, var_kvaser_usb_probe_32_p1)
dev = devm_kzalloc(&intf->dev, sizeof(*dev), GFP_KERNEL);
	for (i = 0; i < dev->nchannels; i++) {
		err = kvaser_usb_init_one(intf, id, i);
		if (err) {
			kvaser_usb_remove_interfaces(dev);
			return err;
		}
	}
not assignment to var_group3(uninitialized)
what do we know about dev->nchannels?

- kvaser_usb_setup_rx_urbs(struct kvaser_usb *dev)
	usb_fill_bulk_urb(..., kvaser_usb_read_bulk_callback,  dev);

- kvaser_usb_read_bulk_callback(struct urb *urb)
	dev = urb->context;
	while (pos <= urb->actual_length - MSG_HEADER_LEN) {
	msg = urb->transfer_buffer + pos;
	kvaser_usb_handle_message(dev, msg);
	pos += msg->len;
	｝
	usb_fill_bulk_urb;
	usb_submit_urb;

-kvaser_usb_rx_error(dev,msg)
	priv = dev->nets[channel];
	stats = &priv->netdev->stats;
if (status & M16C_STATE_BUS_OFF) {
...
	if (!priv->can.restart_ms)
```
