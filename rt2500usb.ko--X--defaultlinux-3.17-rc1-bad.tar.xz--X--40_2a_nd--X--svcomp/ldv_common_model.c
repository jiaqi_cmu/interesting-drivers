
 #include <linux/types.h>
 #include <verifier/rcv.h>
 extern int* nd_ptr(void);
 void* ptr;
 bool filled;
 /* LDV_COMMENT_MODEL_FUNCTION_DEFINITION(name='ldv_usb_fill_urb') URB request initialization */
 void ldv_usb_fill_urb(struct urb *urb) {
  if (urb==ptr) filled=true;
  }

 /* LDV_COMMENT_MODEL_FUNCTION_DEFINITION(name='ldv_usb_submit_urb') URB request submitting */
 int ldv_usb_submit_urb(struct urb *urb) {
  if (urb==ptr)
  {
   ldv_assert(filled);
   filled=false;
  }
  return 1;
 }
 /* LDV_COMMENT_MODEL_FUNCTION_DEFINITION(name='ldv_usb_alloc_urb_nd') URB request allocation */

 /* LDV_COMMENT_MODEL_FUNCTION_DEFINITION(name='ldv_initialize') Initialization of buffer */
 void ldv_initialize(void) {
  /* LDV_COMMENT_CHANGE_STATE All module reference counters have some initial value at the beginning */
  ptr = nd_ptr();
  filled=false;
 }

