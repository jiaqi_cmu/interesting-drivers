#define LDV_MIN(X,Y) ((X) < (Y) ? (X) : (Y))
#define LDV_MAX(X,Y) ((X) > (Y) ? (X) : (Y))
#define LDV_ABS(X) ((X) < 0 ? -(X) : (X))
#define LDV_LROTATE(X,Y) (((X) << (Y)) | ((X) >> (__CHAR_BIT__ * sizeof (X) - Y)))
#define LDV_RROTATE(X,Y) (((X) >> (Y)) | ((X) << (__CHAR_BIT__ * sizeof (X) - Y)))
typedef struct __va_list_tag __va_list_tag;
#line 19 "include/uapi/asm-generic/int-ll64.h"
typedef signed char __s8;
typedef unsigned char __u8;
#line 22 "include/uapi/asm-generic/int-ll64.h"
typedef short int __s16;
typedef short unsigned int __u16;
#line 25 "include/uapi/asm-generic/int-ll64.h"
typedef int __s32;
typedef unsigned int __u32;
#line 29 "include/uapi/asm-generic/int-ll64.h"
typedef long long int __s64;
typedef long long unsigned int __u64;
#line 15 "include/asm-generic/int-ll64.h"
typedef signed char s8;
typedef unsigned char u8;
#line 18 "include/asm-generic/int-ll64.h"
typedef short int s16;
typedef short unsigned int u16;
#line 21 "include/asm-generic/int-ll64.h"
typedef int s32;
typedef unsigned int u32;
#line 24 "include/asm-generic/int-ll64.h"
typedef long long int s64;
typedef long long unsigned int u64;
struct ftrace_branch_data
{
  char const
#line 80 "include/linux/compiler.h"
  *func;
  char const *file;
  unsigned int line;
  union
  {
    struct
    {
      long unsigned int
#line 85 "include/linux/compiler.h"
      correct;
      long unsigned int incorrect;
    };
    struct
    {
      long unsigned int
#line 89 "include/linux/compiler.h"
      miss;
      long unsigned int hit;
    };
    long unsigned int miss_hit[2U];
  };
};
enum ldv_513
{
  false = 0,
  true = 1
};
#line 26 "./include/uapi/linux/posix_types.h"
typedef struct
{
  long unsigned int
#line 25 "./include/uapi/linux/posix_types.h"
  fds_bits[16U];
} __kernel_fd_set;
#line 29 "./include/uapi/linux/posix_types.h"
typedef void (*__kernel_sighandler_t)(int);
#line 32 "./include/uapi/linux/posix_types.h"
typedef int __kernel_key_t;
typedef int __kernel_mqd_t;
#line 10 "./arch/x86/include/uapi/asm/posix_types_64.h"
typedef short unsigned int __kernel_old_uid_t;
typedef short unsigned int __kernel_old_gid_t;
#line 14 "./arch/x86/include/uapi/asm/posix_types_64.h"
typedef long unsigned int __kernel_old_dev_t;
#line 14 "./include/uapi/asm-generic/posix_types.h"
typedef long int __kernel_long_t;
typedef long unsigned int __kernel_ulong_t;
#line 19 "./include/uapi/asm-generic/posix_types.h"
typedef __kernel_ulong_t __kernel_ino_t;
#line 23 "./include/uapi/asm-generic/posix_types.h"
typedef unsigned int __kernel_mode_t;
#line 27 "./include/uapi/asm-generic/posix_types.h"
typedef int __kernel_pid_t;
#line 31 "./include/uapi/asm-generic/posix_types.h"
typedef int __kernel_ipc_pid_t;
#line 35 "./include/uapi/asm-generic/posix_types.h"
typedef unsigned int __kernel_uid_t;
typedef unsigned int __kernel_gid_t;
#line 40 "./include/uapi/asm-generic/posix_types.h"
typedef __kernel_long_t __kernel_suseconds_t;
#line 44 "./include/uapi/asm-generic/posix_types.h"
typedef int __kernel_daddr_t;
#line 48 "./include/uapi/asm-generic/posix_types.h"
typedef unsigned int __kernel_uid32_t;
typedef unsigned int __kernel_gid32_t;
#line 71 "./include/uapi/asm-generic/posix_types.h"
typedef __kernel_ulong_t __kernel_size_t;
typedef __kernel_long_t __kernel_ssize_t;
typedef __kernel_long_t __kernel_ptrdiff_t;
#line 80 "./include/uapi/asm-generic/posix_types.h"
typedef struct
{
  int
#line 79 "./include/uapi/asm-generic/posix_types.h"
  val[2U];
} __kernel_fsid_t;
#line 86 "./include/uapi/asm-generic/posix_types.h"
typedef __kernel_long_t __kernel_off_t;
typedef long long int __kernel_loff_t;
typedef __kernel_long_t __kernel_time_t;
typedef __kernel_long_t __kernel_clock_t;
typedef int __kernel_timer_t;
typedef int __kernel_clockid_t;
typedef char *__kernel_caddr_t;
typedef short unsigned int __kernel_uid16_t;
typedef short unsigned int __kernel_gid16_t;
#line 32 "include/uapi/linux/types.h"
typedef __u16 __le16;
typedef __u16 __be16;
typedef __u32 __le32;
typedef __u32 __be32;
typedef __u64 __le64;
typedef __u64 __be64;
#line 39 "include/uapi/linux/types.h"
typedef __u16 __sum16;
typedef __u32 __wsum;
#line 12 "include/linux/types.h"
typedef __u32 __kernel_dev_t;
#line 14 "include/linux/types.h"
typedef __kernel_fd_set fd_set;
typedef __kernel_dev_t dev_t;
typedef __kernel_ino_t ino_t;
typedef __kernel_mode_t mode_t;
typedef short unsigned int umode_t;
typedef __u32 nlink_t;
typedef __kernel_off_t off_t;
typedef __kernel_pid_t pid_t;
typedef __kernel_daddr_t daddr_t;
typedef __kernel_key_t key_t;
typedef __kernel_suseconds_t suseconds_t;
typedef __kernel_timer_t timer_t;
typedef __kernel_clockid_t clockid_t;
typedef __kernel_mqd_t mqd_t;
#line 29 "include/linux/types.h"
typedef _Bool bool;
#line 31 "include/linux/types.h"
typedef __kernel_uid32_t uid_t;
typedef __kernel_gid32_t gid_t;
typedef __kernel_uid16_t uid16_t;
typedef __kernel_gid16_t gid16_t;
#line 36 "include/linux/types.h"
typedef long unsigned int uintptr_t;
#line 40 "include/linux/types.h"
typedef __kernel_old_uid_t old_uid_t;
typedef __kernel_old_gid_t old_gid_t;
#line 45 "include/linux/types.h"
typedef __kernel_loff_t loff_t;
#line 54 "include/linux/types.h"
typedef __kernel_size_t size_t;
#line 59 "include/linux/types.h"
typedef __kernel_ssize_t ssize_t;
#line 64 "include/linux/types.h"
typedef __kernel_ptrdiff_t ptrdiff_t;
#line 69 "include/linux/types.h"
typedef __kernel_time_t time_t;
#line 74 "include/linux/types.h"
typedef __kernel_clock_t clock_t;
#line 79 "include/linux/types.h"
typedef __kernel_caddr_t *caddr_t;
#line 83 "include/linux/types.h"
typedef unsigned char u_char;
typedef short unsigned int u_short;
typedef unsigned int u_int;
typedef long unsigned int u_long;
#line 89 "include/linux/types.h"
typedef unsigned char unchar;
typedef short unsigned int ushort;
typedef unsigned int uint;
typedef long unsigned int ulong;
#line 97 "include/linux/types.h"
typedef __u8 u_int8_t;
typedef __s8 int8_t;
typedef __u16 u_int16_t;
typedef __s16 int16_t;
typedef __u32 u_int32_t;
typedef __s32 int32_t;
#line 106 "include/linux/types.h"
typedef __u8 uint8_t;
typedef __u16 uint16_t;
typedef __u32 uint32_t;
#line 111 "include/linux/types.h"
typedef __u64 uint64_t;
typedef __u64 u_int64_t;
typedef __s64 int64_t;
#line 133 "include/linux/types.h"
typedef long unsigned int sector_t;
typedef long unsigned int blkcnt_t;
#line 147 "include/linux/types.h"
typedef u64 dma_addr_t;
#line 158 "include/linux/types.h"
typedef unsigned int gfp_t;
typedef unsigned int fmode_t;
typedef unsigned int oom_flags_t;
#line 163 "include/linux/types.h"
typedef u64 phys_addr_t;
#line 168 "include/linux/types.h"
typedef phys_addr_t resource_size_t;
#line 174 "include/linux/types.h"
typedef long unsigned int irq_hw_number_t;
#line 178 "include/linux/types.h"
typedef struct
{
  int
#line 177 "include/linux/types.h"
  counter;
} atomic_t;
#line 183 "include/linux/types.h"
typedef struct
{
  long int
#line 182 "include/linux/types.h"
  counter;
} atomic64_t;
struct list_head
{
  struct list_head
#line 187 "include/linux/types.h"
  *next;
  struct list_head
#line 187 "include/linux/types.h"
  *prev;
};
struct hlist_head
{
  struct hlist_node *first;
};
struct hlist_node
{
  struct hlist_node *next;
  struct hlist_node
#line 195 "include/linux/types.h"
  **pprev;
};
struct ustat
{
  __kernel_daddr_t f_tfree;
  __kernel_ino_t f_tinode;
  char f_fname[6U];
  char f_fpack[6U];
};
struct callback_head
{
  struct callback_head
#line 211 "include/linux/types.h"
  *next;
  void (*func)(struct callback_head *);
};
#line 10 "/ldv/inst/kernel-rules/verifier/rcv.h"
static inline void ldv_error(void)
{
  LDV_ERROR:;
#line 12 "/ldv/inst/kernel-rules/verifier/rcv.h"
  goto LDV_ERROR;
}
#line 20 "/ldv/inst/kernel-rules/verifier/rcv.h"
static inline void ldv_stop(void)
{
#line 21 "/ldv/inst/kernel-rules/verifier/rcv.h"
  LDV_STOP:;
#line 21 "/ldv/inst/kernel-rules/verifier/rcv.h"
  goto LDV_STOP;
}
#line 25 "/ldv/inst/kernel-rules/verifier/rcv.h"
int ldv_undef_int(void);
void *ldv_undef_ptr(void);
long unsigned int ldv_undef_ulong(void);
long int ldv_undef_long(void);
#line 30 "/ldv/inst/kernel-rules/verifier/rcv.h"
static inline int ldv_undef_int_negative(void)
{
  int ret = ldv_undef_int ( );
#line 34 "/ldv/inst/kernel-rules/verifier/rcv.h"
  if (ret >= 0)
  {
#line 34 "/ldv/inst/kernel-rules/verifier/rcv.h"
    ldv_stop ( );
  }
  else
    ( void ) 0;
#line 36 "/ldv/inst/kernel-rules/verifier/rcv.h"
  return ret;
}
#line 39 "/ldv/inst/kernel-rules/verifier/rcv.h"
static inline int ldv_undef_int_nonpositive(void)
{
  int ret = ldv_undef_int ( );
#line 43 "/ldv/inst/kernel-rules/verifier/rcv.h"
  if (ret > 0)
  {
#line 43 "/ldv/inst/kernel-rules/verifier/rcv.h"
    ldv_stop ( );
  }
  else
    ( void ) 0;
#line 45 "/ldv/inst/kernel-rules/verifier/rcv.h"
  return ret;
}
#line 4 "/work/work/current--X--drivers/usb/misc/ftdi-elan.ko--X--defaultlinux-3.17-rc1-bad.tar.xz--X--40_2a_nd--X--svcomp/linux-3.17-rc1-bad.tar.xz/csd_deg_dscv/18/dscv_tempdir/rule-instrumentor/40_2a_nd/common-model/ldv_common_model.c"
int *nd_ptr(void);
void *ptr;
bool filled;
#line 8 "/work/work/current--X--drivers/usb/misc/ftdi-elan.ko--X--defaultlinux-3.17-rc1-bad.tar.xz--X--40_2a_nd--X--svcomp/linux-3.17-rc1-bad.tar.xz/csd_deg_dscv/18/dscv_tempdir/rule-instrumentor/40_2a_nd/common-model/ldv_common_model.c"
void ldv_usb_fill_urb(struct urb *urb)
{
#line 9 "/work/work/current--X--drivers/usb/misc/ftdi-elan.ko--X--defaultlinux-3.17-rc1-bad.tar.xz--X--40_2a_nd--X--svcomp/linux-3.17-rc1-bad.tar.xz/csd_deg_dscv/18/dscv_tempdir/rule-instrumentor/40_2a_nd/common-model/ldv_common_model.c"
  if (( void *) urb == ptr)
#line 9 "/work/work/current--X--drivers/usb/misc/ftdi-elan.ko--X--defaultlinux-3.17-rc1-bad.tar.xz--X--40_2a_nd--X--svcomp/linux-3.17-rc1-bad.tar.xz/csd_deg_dscv/18/dscv_tempdir/rule-instrumentor/40_2a_nd/common-model/ldv_common_model.c"
    filled = 1;
}
#line 13 "/work/work/current--X--drivers/usb/misc/ftdi-elan.ko--X--defaultlinux-3.17-rc1-bad.tar.xz--X--40_2a_nd--X--svcomp/linux-3.17-rc1-bad.tar.xz/csd_deg_dscv/18/dscv_tempdir/rule-instrumentor/40_2a_nd/common-model/ldv_common_model.c"
int ldv_usb_submit_urb(struct urb *urb)
{
#line 14 "/work/work/current--X--drivers/usb/misc/ftdi-elan.ko--X--defaultlinux-3.17-rc1-bad.tar.xz--X--40_2a_nd--X--svcomp/linux-3.17-rc1-bad.tar.xz/csd_deg_dscv/18/dscv_tempdir/rule-instrumentor/40_2a_nd/common-model/ldv_common_model.c"
  if (( void *) urb == ptr)
  {
    if (( int ) filled)
    {
      ( void ) 0;
    }
    else
#line 16 "/work/work/current--X--drivers/usb/misc/ftdi-elan.ko--X--defaultlinux-3.17-rc1-bad.tar.xz--X--40_2a_nd--X--svcomp/linux-3.17-rc1-bad.tar.xz/csd_deg_dscv/18/dscv_tempdir/rule-instrumentor/40_2a_nd/common-model/ldv_common_model.c"
      ldv_error ( );
    filled = 0;
  }
  return 1;
}
#line 24 "/work/work/current--X--drivers/usb/misc/ftdi-elan.ko--X--defaultlinux-3.17-rc1-bad.tar.xz--X--40_2a_nd--X--svcomp/linux-3.17-rc1-bad.tar.xz/csd_deg_dscv/18/dscv_tempdir/rule-instrumentor/40_2a_nd/common-model/ldv_common_model.c"
void ldv_initialize(void)
{
  ptr = ( void *) nd_ptr ( );
  filled = 0;
}
