
 #include <linux/types.h>
 #include <verifier/rcv.h>
 #include <verifier/set.h>

 Set LDV_USB_URB_BUFS;

 /* LDV_COMMENT_MODEL_FUNCTION_DEFINITION(name='ldv_usb_fill_urb') URB request initialization */
 void ldv_usb_fill_urb(struct urb *urb) {
  ldv_set_add(LDV_USB_URB_BUFS, urb);
 }

 /* LDV_COMMENT_MODEL_FUNCTION_DEFINITION(name='ldv_usb_submit_urb') URB request submitting */
 int ldv_usb_submit_urb(struct urb *urb) {
  /* LDV_COMMENT_ASSERT LDV_USB_URB_BUFS are empty */
  ldv_assert(ldv_set_contains(LDV_USB_URB_BUFS, urb));

  ldv_set_init(LDV_USB_URB_BUFS);

  return 1;
 }

 /* LDV_COMMENT_MODEL_FUNCTION_DEFINITION(name='ldv_initialize') Initialization of buffer */
 void ldv_initialize(void) {
  /* LDV_COMMENT_CHANGE_STATE All module reference counters have some initial value at the beginning */
  ldv_set_init(LDV_USB_URB_BUFS);

 }

